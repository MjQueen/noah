package com.noah.exception;


import com.noah.base.Result;
import org.springframework.http.HttpStatus;

import javax.security.sasl.AuthenticationException;

/**
 * 验证码相关异常类
 * @Auther: 梦学谷 www.mengxuegu.com
 */
public class ValidateCodeException extends AuthenticationException {
    public ValidateCodeException(String msg, Throwable t) {
        super(msg, t);
    }

    public  ValidateCodeException(String msg) {
        super(msg);
    }


    public Result ValidateCodeException(String msg) {
        return new Result( HttpStatus.INTERNAL_SERVER_ERROR.value(),msg,"");


    }
}
