package com.noah.security.core.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.noah.entity.SysRoleEntity;

/**
 * @Description 角色业务接口
 * @Author Sans
 * @CreateTime 2019/9/14 15:57
 */
public interface SysRoleService extends IService<SysRoleEntity> {

}