package com.noah.service.impl;

import cn.hutool.core.date.LocalDateTimeUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.noah.base.IpUtil;
import com.noah.base.JwtUtil;
import com.noah.base.Result;
import com.noah.entity.Menu;
import com.noah.entity.SysLoginLog;
import com.noah.entity.SysUser;
import com.noah.mapper.UserMapper;
import com.noah.req.UserREQ;
import com.noah.service.IMenuService;
import com.noah.service.IRoleService;
import com.noah.service.IUserService;
import com.noah.service.SysLoginLogService;
import io.jsonwebtoken.Claims;
import org.apache.commons.lang.StringUtils;
import org.assertj.core.util.Lists;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Jason & Tyrande & Amy
 * @since 2024-01-13
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, SysUser> implements IUserService {
    @Autowired
    private JwtUtil jwtUtils;
    @Autowired
    private IRoleService roleService;

    @Autowired
    private IMenuService menuService;

    @Autowired
    private SysLoginLogService sysLoginLogService;


    @Override
    public Result search(long current, long size, UserREQ req) {
        QueryWrapper<SysUser> query = new QueryWrapper();
        if(req != null) {
            if(StringUtils.isNotBlank(req.getKeyword())) {
                query.lambda().like(SysUser::getNickName,req.getKeyword()).
                        or().like(SysUser::getUsername,req.getKeyword()).
                        or().like(SysUser::getMobile,req.getKeyword());
            }
        }



        // 封装分页对象
        IPage<SysUser> p = new Page<>(current ,size);
        IPage<SysUser> data = baseMapper.selectPage(p, query);
        List<SysUser> records = data.getRecords();
        for (SysUser record : records) {
            List<Integer> roleIdsByUserId = roleService.getRoleIdsByUserId(record.getId());
            String s = roleIdsByUserId.toString();
            record.setRoleIds(roleIdsByUserId);
        }
        data.setRecords(records);

        return Result.ok(data);
    }

    @Transactional
    @Override
    public Result update(Integer id, SysUser user) {
        if(user.getId()==null){
            user.setId(id);
        }
        List<Integer> roleIds = user.getRoleIds();
        for (Integer roleId : roleIds) {
            System.out.println(roleId);
        }
        baseMapper.deleteRolesByUserId(id);
        if(!CollectionUtils.isEmpty(user.getRoleIds())){
            baseMapper.saveRolesByUserId(id,user.getRoleIds());
        }
        int i = baseMapper.updateById(user);
        if(i<1){
            return    Result.error("更新失败");
        }
        return    Result.ok("更新成功");
    }
    @Override
    public SysUser getUserByUserName(String username) {
        QueryWrapper qw = new QueryWrapper();
        qw.eq("username",username);
        SysUser user = baseMapper.selectOne(qw);
        return user;
    }



    @Override
    public Result login(String username, String password) {
        Result error = Result.error("用户名或密码错误");
        if(StringUtils.isBlank(username) || StringUtils.isBlank(password)){
            return error;
        }
        SysUser user = getUserByUserName(username);
        if(user==null){
            return error;
        }

        boolean enabled = user.isEnabled();
        if(!enabled){
            return Result.error("用户不存在");
        }

        //判断输入的密码和数据库密码是否一致
        boolean b = new BCryptPasswordEncoder().matches(password, user.getPassword());
       // boolean b = password.equals(user.getPassword());
        //生成token
        if(!b){
            return error;
        }
        String jwt = jwtUtils.createJWT(user.getId() + "", user.getUsername(), true);

        //返回前端
        Map<String,String> map = new HashMap<>();
        map.put("access_token",jwt);

        //保存登录日志
        RequestAttributes ra = RequestContextHolder.getRequestAttributes();
        ServletRequestAttributes sra = (ServletRequestAttributes) ra;
        HttpServletRequest request = sra.getRequest();
        SysLoginLog sll = new SysLoginLog();
        sll.setUsername(user.getUsername());
        sll.setIpaddr(IpUtil.getIpAddress(request));
        sll.setStatus(0);
        sll.setMsg("登录成功");
        boolean save = sysLoginLogService.save(sll);
        if(!save){
            return Result.error("插入登录日志失败");
        }
        return Result.ok(map);
    }



    @Override
    public Result add(SysUser user) {
        if(user==null ||StringUtils.isBlank(user.getUsername())){
            return Result.error("用户名不能为空");
        }
        SysUser userByUserName = this.getUserByUserName(user.getUsername());
        if(userByUserName!=null){
            return Result.error("用户名已存在");
        }
        String password = new BCryptPasswordEncoder().encode(user.getPassword());
        user.setPassword(password);
        boolean save = this.save(user);
        if(save){
            return Result.ok("新增成功");
        }

        return Result.error("新增失败");
    }

    @Override
    public Result updatePassword(UserREQ userReq) {
        String newPassword = userReq.getNewPassword();
        String reqPassword = userReq.getReqPassword();
        System.out.println(userReq.toString());
        if(StringUtils.isBlank(newPassword) || StringUtils.isBlank(reqPassword)){
            return Result.error("密码错误");
        }
        if(!newPassword.equals(reqPassword)){
            return Result.error("两次密码不一致");
        }
        String encodePassword = new BCryptPasswordEncoder().encode(newPassword);
        SysUser user = baseMapper.selectById(userReq.getUserId());
        user.setPassword(encodePassword);
        baseMapper.updateById(user);

        return Result.ok("修改密码成功");
    }

    @Override
    public int deleteById(Integer id) {
        SysUser user = baseMapper.selectById(id);
        user.setEnabled(false);
        int i = baseMapper.updateById(user);

        return i;
    }



    @Override
    public Result getUserInfoByToken(String token) {
        if(StringUtils.isBlank(token)){
            return Result.error("令牌为空,访问用户数据失败");
        }
        Claims claims = jwtUtils.parseJWT(token);
        if(claims==null||StringUtils.isBlank(claims.getSubject())){
            return Result.error("获取用户令牌失败");
        }

        String userName = claims.getSubject();
        SysUser userInfo = this.getUserByUserName(userName);
        List<Integer> roleIdsByUserId = roleService.getRoleIdsByUserId(userInfo.getId());
        String s = roleIdsByUserId.toString();
        userInfo.setRoleIds(roleIdsByUserId);

        userInfo.setPassword(null);
        userInfo.setCreateTime(null);
        userInfo.setUpdateTime(null);
        List<Menu> menuByUserId = menuService.findMenuByUserId(userInfo.getId());
        menuByUserId.remove(null);
        if(CollectionUtils.isEmpty(menuByUserId)){
            return Result.error("用户没有权限");
        }
        List<Menu> menuList = Lists.newArrayList();
        List<String> buttonList = Lists.newArrayList();

        for (Menu menu : menuByUserId) {
            if(menu.getType()==1){
                menuList.add(menu);
            }else{
                buttonList.add(menu.getCode());
            }
        }
         menuList = menuService.buildTree(menuList);
        Map<String,Object> map = new HashMap<>();
        map.put("userInfo",userInfo);
        map.put("menuList",menuList);
        map.put("buttonList",buttonList);
        return Result.ok(map);
    }



}
