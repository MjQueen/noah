package com.noah.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.noah.base.Result;
import com.noah.entity.Menu;
import com.noah.entity.SysUser;
import com.noah.mapper.MenuMapper;
import com.noah.req.QueryKeyword;
import com.noah.service.IMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang.StringUtils;
import org.assertj.core.util.Lists;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 权限表 服务实现类
 * </p>
 *
 * @author Jason & Tyrande & Amy 
 * @since 2024-02-01
 */
@Service
public class MenuServiceImpl extends ServiceImpl<MenuMapper, Menu> implements IMenuService {

    /**
     * 用于下拉列表分配权限
     * @return
     */
    @Override
    public List<Menu> getDownList() {
        LambdaQueryWrapper<Menu> lw = new LambdaQueryWrapper();
        List<Menu> menus = baseMapper.selectList(lw);
        List<Menu> newMenus = this.buildTree(menus);
        return newMenus;
    }

    @Override
    public Result search(QueryKeyword queryKeyword) {

        LambdaQueryWrapper<Menu> lambdaQueryWrapper = new LambdaQueryWrapper();
        if(queryKeyword != null) {
            if(StringUtils.isNotBlank(queryKeyword.getKeyword())) {
                lambdaQueryWrapper.like(Menu::getTitle,queryKeyword.getKeyword());
            }

        }

        List<Menu> menuList = baseMapper.selectList(lambdaQueryWrapper);

        List<Menu> menus = this.buildTree(menuList);;

        return Result.ok(menus);
    }

    /**
     * 将菜单封装成树状结构
     * @param
     * @return
     */

    public List<Menu> buildTree(List<Menu> list){
        List<Menu> rootList = Lists.newArrayList();
        //1.获取根菜单
        for (Menu menu : list) {
            if(menu.getParentId()==0){
                rootList.add(menu);
            }
        }
        //2.获取根菜单下面的子目录
        for (Menu menu : rootList) {
            childrenMenu(list,menu);
        }

        return rootList;
    }

    public Menu childrenMenu(List<Menu> list, Menu parentmenu) {
        //存放所有menu子菜单的集合
        List<Menu> childrenList = Lists.newArrayList();
        //循环所有菜单，看是否是根菜单的子菜单
        for (Menu m : list) {
            if(m.getParentId().equals(parentmenu.getId())){
                childrenList.add(childrenMenu(list,m));
            }
        }
        parentmenu.setChildren(childrenList);
        return parentmenu;
    }

    @Override
    public List<String> findUserPermsList(Integer userId) {
        if(userId==null){
            return null;
        }
        List<Menu> menus = baseMapper.selectMenuByUserId(userId);
        //创建返回的集合
        List<String> permissionList = new ArrayList<>();
        for (Menu sysMenu : menus) {
            if (sysMenu.getType() == 2) {
                permissionList.add(sysMenu.getCode());
            }
        }

        return permissionList;
    }


    @Override
    public List<Menu> findMenuByUserId(Integer userId) {
        if(userId==null){
            return null;
        }
        List<Menu> menus = baseMapper.selectMenuByUserId(userId);
        menus.remove(null);

        return menus;
    }


}
