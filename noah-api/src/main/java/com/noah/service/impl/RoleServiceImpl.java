package com.noah.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.noah.base.Result;
import com.noah.entity.Menu;
import com.noah.entity.Role;
import com.noah.mapper.RoleMapper;
import com.noah.req.QueryKeyword;
import com.noah.service.IRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.assertj.core.util.Lists;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author Jason & Tyrande & Amy 
 * @since 2024-02-01
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements IRoleService {

    @Override
    public Result search(long current, long size, QueryKeyword queryKeyword) {
        LambdaQueryWrapper<Role> lambdaQueryWrapper = new LambdaQueryWrapper();
        if(queryKeyword != null) {
            if(StringUtils.isNotBlank(queryKeyword.getKeyword())) {
                lambdaQueryWrapper.like(Role::getRoleName,queryKeyword.getKeyword());
            }

        }
        // 封装分页对象
        IPage<Role> p = new Page<>(current ,size);
        IPage<Role> data = baseMapper.selectPage(p, lambdaQueryWrapper);
        return Result.ok(data);
    }

    @Override
    public Result getMenuIdsByRoleId(Integer roleId) {
        List<Integer> menuIdsByRoleId = baseMapper.getMenuIdsByRoleId(roleId);
        return Result.ok(menuIdsByRoleId);
    }

    @Transactional
    @Override
    public Result deleteRoleMenuByRoleId(Integer roleId) {
        boolean b = baseMapper.deleteRoleMenuByRoleId(roleId);
        if(b){
            return Result.ok();
        }
        return Result.error("删除失败");
    }
    @Transactional
    @Override
    public Result saveMenuIdsByRoleId(Integer roleId, List<Integer> menuIds) {
        baseMapper.deleteRoleMenuByRoleId(roleId);
        if(CollectionUtils.isNotEmpty(menuIds)){
            baseMapper.saveRoleMenu(roleId,menuIds);
        }
        return Result.ok();


    }

    @Override
    public Result getRolesList() {
        List<Role> list = Lists.newArrayList();
        List<Integer> rolesList = baseMapper.getRolesList();
        for (Integer s : rolesList) {
            Role role = baseMapper.selectById(s);
            list.add(role);
        }
        return Result.ok(list);
    }


    public List<Integer> getRoleIdsByUserId(Integer userId){
        List<Integer> rolesByUserId = baseMapper.getRolesByUserId(userId);

        return rolesByUserId;
    }

}
