package com.noah.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.noah.base.Result;
import com.noah.entity.Goods;
import com.noah.mapper.GoodsMapper;
import com.noah.req.GoodsREQ;
import com.noah.service.IGoodsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品信息表 服务实现类
 * </p>
 *
 * @author Jason & Tyrande & Amy
 * @since 2024-01-13
 */
@Service
public class GoodsServiceImpl extends ServiceImpl<GoodsMapper, Goods> implements IGoodsService {

    @Override
    public Result search(long page, long size, GoodsREQ req) {
        QueryWrapper<Goods> query = new QueryWrapper<>();
        if (StringUtils.isNotBlank(req.getKeyword())) {
            query.like("name", req.getKeyword());
        }
        Page Ipage = new Page<Goods>(page, size);
        IPage<Goods> data = baseMapper.selectPage(Ipage, query);
        return Result.ok(data);
    }
}
