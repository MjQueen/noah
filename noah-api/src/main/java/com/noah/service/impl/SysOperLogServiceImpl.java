package com.noah.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.noah.base.Result;
import com.noah.entity.Role;
import com.noah.entity.SysOperLog;
import com.noah.mapper.SysOperLogMapper;
import com.noah.req.QueryKeyword;
import com.noah.service.SysOperLogService;
import com.noah.req.SysOperLogQueryVo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author tanlingfei
 * @version 1.0
 * @description 操作日志记录 Service实现类
 * @date 2023-04-30 21:39:39
 */
@Transactional
@Service
public class SysOperLogServiceImpl extends ServiceImpl
        <SysOperLogMapper, SysOperLog> implements SysOperLogService {
    @Autowired
    private SysOperLogMapper sysOperLogMapper;

    @Override
    public IPage<SysOperLog> selectPage(Page<SysOperLog> pageParam, QueryKeyword sysOperLogQueryVo) {
        return sysOperLogMapper.selectPage(pageParam, sysOperLogQueryVo);
    }

    @Override
    public List<SysOperLog> queryList(SysOperLogQueryVo sysOperLogQueryVo) {
        return sysOperLogMapper.queryList(sysOperLogQueryVo);
    }

    @Override
    public boolean save(SysOperLog sysOperLog) {
        int result = this.sysOperLogMapper.insert(sysOperLog);
        return result > 0;
    }

    @Override
    public boolean updateById(SysOperLog sysOperLog) {
        int row = this.sysOperLogMapper.updateById(sysOperLog);
        return row > 0;
    }

    @Override
    public SysOperLog getById(String id) {
        SysOperLog sysOperLog = sysOperLogMapper.selectById(id);
        return sysOperLog;
    }

    @Override
    public List<SysOperLog> getByIds(List<String> ids) {
        List<SysOperLog> list = this.sysOperLogMapper.selectBatchIds(ids);
        return list;
    }

    @Override
    public Result search(QueryKeyword queryKeyword) {
        LambdaQueryWrapper<SysOperLog> lambdaQueryWrapper = new LambdaQueryWrapper();
        if(queryKeyword != null) {
            if(StringUtils.isNotBlank(queryKeyword.getKeyword())) {
                lambdaQueryWrapper.like(SysOperLog::getOperName,queryKeyword.getKeyword());
            }
        }
        lambdaQueryWrapper.orderByDesc(SysOperLog::getCreateTime);
        // 封装分页对象
        IPage<SysOperLog> p = new Page<>(queryKeyword.getCurrent() ,queryKeyword.getSize());
        IPage<SysOperLog> data = baseMapper.selectPage(p, lambdaQueryWrapper);
        return Result.ok(data);
    }
}
