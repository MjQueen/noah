package com.noah.service;

import com.noah.base.Result;
import com.noah.entity.Role;
import com.baomidou.mybatisplus.extension.service.IService;
import com.noah.req.QueryKeyword;

import java.util.List;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author Jason & Tyrande & Amy 
 * @since 2024-02-01
 */
public interface IRoleService extends IService<Role> {

    Result search(long current, long size, QueryKeyword queryKeyword);

    Result getMenuIdsByRoleId(Integer roleId);

    Result deleteRoleMenuByRoleId(Integer roleId);

    Result saveMenuIdsByRoleId(Integer roleId, List<Integer> menuIds);

    Result getRolesList();

    List<Integer> getRoleIdsByUserId(Integer userId);
}
