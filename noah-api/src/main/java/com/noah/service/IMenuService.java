package com.noah.service;

import com.noah.base.Result;
import com.noah.entity.Menu;
import com.baomidou.mybatisplus.extension.service.IService;
import com.noah.req.QueryKeyword;

import java.util.List;

/**
 * <p>
 * 权限表 服务类
 * </p>
 *
 * @author Jason & Tyrande & Amy 
 * @since 2024-02-01
 */
public interface IMenuService extends IService<Menu> {

    Result search(QueryKeyword queryKeyword);

    List<Menu> findMenuByUserId(Integer userId);

    List<Menu> getDownList();

    List<Menu> buildTree(List<Menu> list);

    Menu childrenMenu(List<Menu> list, Menu parentmenu);

    List<String> findUserPermsList(Integer userId);

}
