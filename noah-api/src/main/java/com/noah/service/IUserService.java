package com.noah.service;

import com.noah.base.Result;
import com.noah.entity.Menu;
import com.noah.entity.SysUser;
import com.baomidou.mybatisplus.extension.service.IService;
import com.noah.req.UserREQ;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Jason & Tyrande & Amy
 * @since 2024-01-13
 */
public interface IUserService extends IService<SysUser> {
    //分页条件查询

    Result search(long current, long size, UserREQ req);

    Result update(Integer id, SysUser user);
    Result login (String userName,String password);


    SysUser getUserByUserName(String userName);

    Result getUserInfoByToken(String token);

    Result add(SysUser user);


    Result updatePassword(UserREQ userReq);

    int deleteById(Integer id);


}
