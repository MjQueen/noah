package com.noah.service;

import com.noah.base.Result;
import com.noah.entity.Goods;
import com.baomidou.mybatisplus.extension.service.IService;
import com.noah.req.GoodsREQ;

/**
 * <p>
 * 商品信息表 服务类
 * </p>
 *
 * @author Jason & Tyrande & Amy
 * @since 2024-01-13
 */
public interface IGoodsService extends IService<Goods> {

    /**
     * 分页条件查询
     * @param page 当前页码
     * @param size 每页条数
     * @param req 查询条件参数
     * @return
     */
    Result search(long page, long size, GoodsREQ req);
}
