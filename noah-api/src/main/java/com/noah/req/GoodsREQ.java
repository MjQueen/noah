package com.noah.req;

import lombok.Data;

import java.io.Serializable;

@Data
public class GoodsREQ implements Serializable {

    /**
     * 商品名称
     */
    private String keyword;

    private Integer status;
}
