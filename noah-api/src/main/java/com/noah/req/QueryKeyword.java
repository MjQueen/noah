package com.noah.req;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author Jason & Tyrande & Amy
 * @since 2024-01-13
 */
@Data
public class QueryKeyword implements Serializable {

    private static final long serialVersionUID = 1L;

    private String keyword;
    private long current;
    private long size;
}
