package com.noah.req;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class UserREQ implements Serializable {
    private String keyword;
    private long current;
    private long size;

    //修改密码
    private String newPassword;
    private String reqPassword;
    private Integer userId;

    /**
     * id自增
     */

    private Integer id;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 密码
     */
    private String password;

    /**
     * 花名
     */
    private String nickName;

    /**
     * 电话
     */
    private String mobile;

    /**
     * 角色
     */

    private String roleIds;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 头像
     */
    private String imageUrl;

    /**
     * 备注
     */
    private String remark;

    /**
     * 创建日期
     */
    private Date createTime;

    /**
     * 更新日期
     */
    private Date updateTime;

    /**
     * 账号锁定
     */
    private Boolean accountNonLocked;

    /**
     * 账号过期
     */
    private Boolean accountNonExpired;

    /**
     * 密码过期
     */
    private Boolean credentialsNonExpired;





}
