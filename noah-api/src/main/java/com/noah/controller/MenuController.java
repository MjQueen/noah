package com.noah.controller;



import com.noah.entity.Menu;
import com.noah.entity.Meta;
import com.noah.log.annotation.Log;
import com.noah.log.type.BusinessType;
import com.noah.req.QueryKeyword;
import com.noah.service.IMenuService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 权限表 前端控制器
 * </p>
 *
 * @author Jason & Tyrande & Amy 
 * @since 2024-02-01
 */
@RestController
@RequestMapping("/system/menu")
public class MenuController {
    Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private IMenuService menuService;


    /**
     * 增删改查
     * @param
     * @return
     */


    @PostMapping("/search")
    public Result search(@RequestBody QueryKeyword queryKeyword) {
        String keyword = queryKeyword.getKeyword();
        logger.info("关键词", keyword);
        return menuService.search(queryKeyword);
    }

    @PostMapping()
    @Log(title = "新增菜单信息" ,businessType = BusinessType.INSERT)
    public Result add(@RequestBody Menu menu){
        Meta meta = menu.getMeta();
        menu.setTitle(meta.getTitle());
        menu.setCache(meta.getCache());
        menu.setHidden(meta.getHidden());
        menu.setIcon(meta.getIcon());
        menu.setIsBreadcrumd(meta.getIsBreadcrumd());

        boolean b = menuService.save(menu);
        if (!b) {
            return Result.error("新增失败");
        }
        return Result.ok("新增成功");

    }

    @DeleteMapping("/{id}")
    @Log(title = "删除菜单信息" ,businessType = BusinessType.DELETE)
    public  Result deleteById(@PathVariable("id") Integer id){
        boolean delete = menuService.removeById(id);
        if(delete){
            return Result.ok("删除成功");
        }
        return Result.error("删除失败");
    }

        @PutMapping()
         @Log(title = "修改菜单信息" ,businessType = BusinessType.UPDATE)
        public Result update( @RequestBody Menu menu){
        Meta meta = menu.getMeta();
        menu.setTitle(meta.getTitle());
        menu.setCache(meta.getCache());
        menu.setHidden(meta.getHidden());
        menu.setIcon(meta.getIcon());
        menu.setIsBreadcrumd(meta.getIsBreadcrumd());

            boolean b = menuService.updateById(menu);
            if (!b) {
                return Result.error("更新失败");
            }
                return Result.ok("更新成功");

        }


    /**查询所有菜单的所有数据 */
    @GetMapping("/getDownList")
    public Result getDownList() {
        List<Menu> list = menuService.getDownList();

        return Result.ok(list);
    }




    /**查询类型为菜单的所有数据 */
        @GetMapping("/select")
        public Result getMenuSelect() {
            List<Menu> list = menuService.list();
            for (Menu menu : list) {
                if(menu.getParentId()!=null || menu.getParentId()!=0){
                    Menu parentMenu = menuService.getById(menu.getParentId());


                }
            }
            return Result.ok();
        }




}
