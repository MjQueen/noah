package com.noah.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.noah.base.Result;
import com.noah.entity.SysOperLog;
import com.noah.req.QueryKeyword;
import com.noah.service.SysOperLogService;
import com.noah.req.SysOperLogQueryVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author tanlingfei
 * @version 1.0
 * @description 操作日志记录
 * @date 2023-04-30 21:39:39
 */
@Api(tags = "操作日志记录")
@RestController
@RequestMapping("/log/sysOperLog")
public class SysOperLogController {
    @Autowired
    private SysOperLogService sysOperLogService;

    @PostMapping()
    public Result search(@RequestBody QueryKeyword queryKeyword) {
        return sysOperLogService.search(queryKeyword);
    }



    @ApiOperation(value = "保存操作日志记录")
    @PostMapping("/save")
    public Result save(@RequestBody SysOperLog sysOperLog) {
        sysOperLogService.save(sysOperLog);
        return Result.ok();
    }


}
