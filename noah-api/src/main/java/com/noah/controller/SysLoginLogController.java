package com.noah.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.noah.base.Result;
import com.noah.entity.SysLoginLog;
import com.noah.req.QueryKeyword;
import com.noah.service.SysLoginLogService;
import com.noah.req.SysLoginLogQueryVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author tanlingfei
 * @version 1.0
 * @description 系统访问记录
 * @date 2023-04-30 21:36:41
 */
@Api(tags = "系统访问记录")
@RestController
@RequestMapping("/log/sysLoginLog")
public class SysLoginLogController {
    @Autowired
    private SysLoginLogService sysLoginLogService;


    @ApiOperation(value = "获取分页列表")
    @PostMapping("")
    public Result search(@RequestBody QueryKeyword queryKeyword) {
        return sysLoginLogService.search(queryKeyword);
    }


    @ApiOperation(value = "保存系统访问记录")
    @PostMapping("/save")
    public Result save(@RequestBody SysLoginLog sysLoginLog) {
        sysLoginLogService.save(sysLoginLog);
        return Result.ok();
    }
}
