package com.noah.controller;


import com.noah.base.Result;
import com.noah.entity.Role;
import com.noah.log.annotation.Log;

import com.noah.log.type.BusinessType;

import com.noah.req.QueryKeyword;
import com.noah.req.UserREQ;
import com.noah.service.IRoleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 角色表 前端控制器
 * </p>
 *
 * @author Jason & Tyrande & Amy 
 * @since 2024-02-01
 */
@RestController
@RequestMapping("/system/role")
public class RoleController {
    Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    private IRoleService roleService;
    /**
     * 增删改查
     * @param
     * @return
     */

    @PostMapping("/search")
    /*@Log(title = "查询角色信息" ,businessType = BusinessType.QUERY)*/
    public Result search(@RequestBody QueryKeyword queryKeyword) {
        long current = queryKeyword.getCurrent();
        long size = queryKeyword.getSize();
        String keyword = queryKeyword.getKeyword();
        logger.info("关键词", keyword);
        return roleService.search(current,size,queryKeyword);
    }

        @PostMapping()
        @Log(title = "新增角色信息" ,businessType = BusinessType.INSERT)
        public Result add(@RequestBody Role role){
            boolean b = roleService.save(role);
            if (!b) {
                return Result.error("新增失败");
            }
            return Result.ok("新增成功");

        }

        @DeleteMapping("/{id}")
        @Log(title = "删除角色信息" ,businessType = BusinessType.DELETE)
        public  Result deleteById(@PathVariable("id") Integer id){
            boolean delete = roleService.removeById(id);
            if(delete){
                return Result.ok("删除成功");
            }
            return Result.error("删除失败");
        }

        @PutMapping()
        @Log(title = "修改角色信息" ,businessType = BusinessType.UPDATE)
        public Result update( @RequestBody Role role){
                boolean b = roleService.updateById(role);
                if (!b) {
                    return Result.error("更新失败");
                }
                    return Result.ok("更新成功");

            }

    @GetMapping("/{roleId}/menu/ids")
    public  Result getMenuIdsByRoleId(@PathVariable Integer roleId){
       return roleService.getMenuIdsByRoleId(roleId);
    }

    @PostMapping ("/{roleId}/menu/ids")
    @Log(title = "为角色分配权限" ,businessType = BusinessType.ASSGIN)
    public  Result saveMenuIdsByRoleId(@PathVariable Integer roleId,
                                       @RequestBody List<Integer> menuIds){
        return roleService.saveMenuIdsByRoleId(roleId,menuIds);
    }

    @GetMapping("/list")
    public Result getRolesList(){
        return roleService.getRolesList();
    }

}
