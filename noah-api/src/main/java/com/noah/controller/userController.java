package com.noah.controller;

import com.google.code.kaptcha.impl.DefaultKaptcha;
import com.noah.base.Result;
import com.noah.base.VerifyCode;
import com.noah.entity.SysUser;
import com.noah.log.annotation.Log;

import com.noah.log.type.BusinessType;

import com.noah.req.UserREQ;
import com.noah.service.IUserService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.*;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Jason & Tyrande & Amy
 * @since 2024-01-13
 */

@RestController
@RequestMapping("/system/user")
public class userController {

    public static final String SESSION_KEY = "SESSION_KEY_IMAGE_CODE";
    Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    private IUserService userService;
    @Autowired
    private DefaultKaptcha defaultKaptcha;
//    @Autowired
//    private UserDetailsService userDetailsServiceImpl;

    /**
     * 增删改查
     * @param req
     * @return
     */


    @PostMapping("/search")
//    @Log(title = "查询用户信息" ,businessType = BusinessType.QUERY)
    public Result search( @RequestBody UserREQ req) {
        long current = req.getCurrent();
        long size = req.getSize();
        logger.info("查询用户列表：current={}, size={}", current, size);

        return userService.search(current,size,req);
    }
    @PostMapping
    @Log(title = "新增用户信息" ,businessType = BusinessType.INSERT)
    public  Result add(@RequestBody SysUser user){
        return userService.add(user);
    }

    @DeleteMapping("/{id}")
    @Log(title = "删除用户信息" ,businessType = BusinessType.DELETE)
    public  Result deleteById(@PathVariable("id") Integer id){
        int delete = userService.deleteById(id);
        if(delete>0){
            return Result.ok("删除成功");
        }
        return Result.error("删除失败");
    }

    @PutMapping()
    @Log(title = "修改用户信息" ,businessType = BusinessType.UPDATE)
    public  Result put(@RequestBody SysUser user){
        return userService.update(user.getId(),user);

    }





    @GetMapping("/{id}")
    public  Result get(@PathVariable("id") int id){
        SysUser byId = userService.getById(id);
        return Result.ok(byId);
    }

    @GetMapping("/exist")//get请求，没有requestBody,直接写参数名称类型即可
    public  Result check(String userName,
                          String mobile){
        if(userName==null || StringUtils.isBlank(userName)){
            SysUser existMobile = userService.lambdaQuery()
                    .eq(SysUser::getMobile, mobile)
                    .one();
            if(existMobile !=null){
                return Result.error("手机号已存在");
            }
        }else{
            SysUser user = userService.getUserByUserName(userName);
            if(user !=null){
                return Result.error("用户名已被注册");
            }
        }

        return Result.ok();

    }

    @PostMapping("/auth/login")
    public Result login( @RequestParam String username,
                         @RequestParam String password) {

       return userService.login(username, password);
    }


    //获取图形验证码
    @GetMapping("/code/image")
    public void imageCode(HttpServletRequest request, HttpServletResponse response) throws IOException {
        //1.获取验证码字符串
        String code = defaultKaptcha.createText();
        //把字符串放到session中
        request.getSession().setAttribute(SESSION_KEY,code);

        //验证码
        System.out.println(code); //获取验证码图片
        BufferedImage image = defaultKaptcha.createImage(code);
        //将验证码图片写出去
        ServletOutputStream outputStream = response.getOutputStream();
        ImageIO.write(image,"jpg",outputStream);

    }
        //修改密码
    @PutMapping("/password")
    @Log(title = "修改用户密码" ,businessType = BusinessType.UPDATE)
    public  Result put(@RequestBody UserREQ userReq){
        return userService.updatePassword(userReq);

    }



    //获取用户信息和菜单和权限
    @GetMapping("/menu")
    public Result getUserInfoBytoken(@RequestParam String token){
        return   userService.getUserInfoByToken(token);

    }


}