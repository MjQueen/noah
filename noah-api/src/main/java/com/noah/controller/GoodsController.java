package com.noah.controller;


import com.noah.entity.Goods;
import com.noah.req.GoodsREQ;
import com.noah.service.IGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 商品信息表 前端控制器
 * </p>
 *
 * @author Jason & Tyrande & Amy
 * @since 2024-01-13
 */
@RestController
@RequestMapping("/goods")
public class GoodsController {

    @Autowired
    private IGoodsService goodsService;

    @PostMapping("/search/{page}/{size}")
    private Result search(
            @PathVariable("page") long page,
            @PathVariable("size") long size,
            @RequestBody(required = false) GoodsREQ comeReq
    ) {

        System.out.println("--------------------Modify-----------------------");
        return goodsService.search(page, size, comeReq);
    }

    @PostMapping
    private Result add(@RequestBody Goods goods) {
        boolean b = goodsService.save(goods);
        if (!b) {
            return Result.error("新增失败");
        }
        ;
        return Result.ok("新增成功");
    }

    @DeleteMapping("/{id}")
    private Result delete(@PathVariable("id") int id) {
        boolean b = goodsService.removeById(id);
        if (!b) {
            return Result.error("刪除失敗");
        }
        return Result.ok("刪除成功");
    }

    @PutMapping()
    private Result update( @RequestBody Goods goods){
        boolean b = goodsService.updateById(goods);
        if (!b) {
            return Result.error("更新失败");
        }
            return Result.ok("更新成功");
    }

}
