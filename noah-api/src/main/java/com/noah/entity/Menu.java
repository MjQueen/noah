package com.noah.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.time.LocalDateTime;
import java.util.Date;
import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.sun.org.apache.xpath.internal.operations.Bool;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 权限表
 * </p>
 *
 * @author Jason & Tyrande & Amy 
 * @since 2024-02-01
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("tb_menu")
public class Menu implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 权限 ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 上级菜单
     */
    private Integer parentId;

    /**
     * 上级菜单名
     */
    @TableField(exist = false)
    private String parentName;

    /**
     * 菜单类型(1菜单，2按钮)
     */
    private Integer type;

    /**
     * 路由地址
     */
    private String path;

    /**
     * 路由名称
     */
    private String name;

    /**
     * 授权标识
     */
    private String code;

    /**
     * 重定向
     */
    private String redirect;

    /**
     * 组件路径
     */
    private String component;

    /**
     * 菜单名称
     */
    private String title;

    /**
     * 图标
     */
    private String icon;

    /**
     * 是否缓存
     */
    private Boolean cache;

    /**
     * 是否隐藏
     */
    private Boolean hidden;

    /**
     * 面包屑
     */
    @TableField("isBreadcrumd")
    private Boolean isBreadcrumd;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 备注
     */
    private String remark;

    /**
     * 创建日期
     */
    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    /**
     * 更新日期
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;

    /**
     * 子菜单
     */
    @TableField(exist = false)
    private List<Menu> children;

    /**
     * 子菜单路由
     */
    @TableField(exist = false)
    private List<String> childrenUrl;
    /**
     * Meta对象
     */
    @TableField(exist = false)
    private Meta meta;

    public Meta getMeta() {
        if(meta==null){
            Meta meta = new Meta();
            meta.setTitle(this.title);
            meta.setCache(this.cache);
            meta.setHidden(this.hidden);
            meta.setIcon(this.icon);
            meta.setIsBreadcrumd(this.isBreadcrumd);
            return meta;
        }

        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }


}
