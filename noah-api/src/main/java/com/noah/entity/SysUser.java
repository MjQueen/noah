package com.noah.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.userdetails.UserDetails;
import org.assertj.core.util.Lists;
import sun.security.provider.PolicyParser;

/**
 * <p>
 * 
 * </p>
 *
 * @author Jason & Tyrande & Amy
 * @since 2024-01-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("tb_user")
public class SysUser  implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id自增
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 花名
     */
    private String nickName;

    /**
     * 电话
     */
    private String mobile;

    /**
     * 角色
     */
    @TableField(exist = false)
    private List<Integer> roleIds= Lists.newArrayList();

    /**
     * 邮箱
     */
    private String email;

    /**
     * 头像
     */
    private String imageUrl;

    /**
     * 备注
     */
    private String remark;

    /**
     * 创建日期
     */
    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    /**
     * 更新日期
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;

    /**
     * 账号锁定
     */
    private boolean isAccountNonLocked;

    /**
     * 账号过期
     */
    private boolean isAccountNonExpired;

    /**
     * 密码过期
     */
    private boolean isCredentialsNonExpired;

    private boolean isEnabled = true;

//    @TableField(exist = false)


//    private Collection<? extends GrantedAuthority> authorities;



//    @TableField(exist=false)
//    private Collection<> authority;

}
