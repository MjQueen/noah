package com.noah.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.collections.CollectionUtils;
import org.assertj.core.util.Lists;


/**
 * <p>
 * 角色表
 * </p>
 *
 * @author Jason & Tyrande & Amy 
 * @since 2024-02-01
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("tb_role")
public class Role implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 角色 ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 角色名称
     */
    private String roleName;

    /**
     * 角色编码
     */
    private String roleCode;

    /**
     * 角色状态
     */
    private boolean status;

    /**
     * 备注信息
     */
    private String remark;

    /**
     * 创建日期
     */
    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    /**
     * 更新日期
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;

    @TableField(exist = false)
    private List<Menu> menuList= Lists.newArrayList();

    @TableField(exist = false)
    private List<Integer> menuIds= Lists.newArrayList();

    public List<Integer> getMenuIds() {
        if (CollectionUtils.isNotEmpty(menuList)) {
            //menuIds = Lists.newArrayList();
            for (Menu menu:menuList) {
                menuIds.add(menu.getId());
            }
        }
        return menuIds;
    }
}
