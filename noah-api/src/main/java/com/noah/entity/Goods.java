package com.noah.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 商品信息表
 * </p>
 *
 * @author Jason & Tyrande & Amy
 * @since 2024-01-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("tb_goods")
public class Goods implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 商品名称
     */
    private String name;

    /**
     * 商品编码
     */
    private String code;

    /**
     * 商品规格
     */
    private String imageUrl;

    /**
     * 零售价
     */
    private String quickCode;

    /**
     * 进货价
     */
    private String specsName;

    /**
     * 库存数量
     */
    private String unitName;

    /**
     * 供应商id
     */
    private String categoryId;

    private String categoryName;

    private BigDecimal cost;

    private BigDecimal stockNum;

    private BigDecimal price;

    private BigDecimal discountPrice;

    private BigDecimal score;

    private String address;

    private String brand;

    private Integer status;

    private Integer sort;

    private String remark;

    private String goodsDetail;

    private Date createTime;

    private Date updateTime;

    private Integer loading;


}
