package com.noah.config;

import com.alibaba.druid.pool.DruidDataSource;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

/**
 * @author Lenovo
 * @date 2023/4/12
 * @time 14:03
 * @project springboot_mybatis
 **/

@Configuration
public class DruidConfig {


    //自定义Druid数据源到spring容器中，不再让springboot自动创建

    /**
     * @ConfigurationProperties(prefix = "spring.datasource")：作用就是将 全局配置文件中
     * 前缀为 spring.datasource的属性值注入到 com.alibaba.druid.pool.DruidDataSource 的同名参数中
     */
    @ConfigurationProperties(prefix = "spring.datasource")

    //注册Bean对象：
    @Bean
    public DataSource druidDataSource(){
        //返回一个druidDataSource;
        return new DruidDataSource();
    }
}