package com.noah.mapper;

import com.noah.entity.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author Jason & Tyrande & Amy 
 * @since 2024-02-01
 */
public interface RoleMapper extends BaseMapper<Role> {
    List<Integer> getMenuIdsByRoleId(@Param("roleId") Integer roleId);

    boolean deleteRoleMenuByRoleId(@Param("roleId") Integer roleId);

    boolean saveRoleMenu(@Param("roleId") Integer roleId,@Param("menuIds") List<Integer> menuIds);

    List<Integer> getRolesList();

    List<Integer> getRolesByUserId(@Param("userId") Integer userId);
}
