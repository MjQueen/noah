package com.noah.mapper;

import com.noah.entity.Menu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 权限表 Mapper 接口
 * </p>
 *
 * @author Jason & Tyrande & Amy 
 * @since 2024-02-01
 */
public interface MenuMapper extends BaseMapper<Menu> {

   List<Menu> selectMenuByUserId(@Param("userId") Integer userId);
}
