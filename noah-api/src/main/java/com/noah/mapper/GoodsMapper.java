package com.noah.mapper;

import com.noah.entity.Goods;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 商品信息表 Mapper 接口
 * </p>
 *
 * @author Jason & Tyrande & Amy
 * @since 2024-01-13
 */
public interface GoodsMapper extends BaseMapper<Goods> {

}
