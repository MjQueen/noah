package com.noah.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.noah.base.Result;
import com.noah.entity.SysUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface UserMapper extends BaseMapper<SysUser> {



    boolean deleteRolesByUserId(@Param("userId")Integer userId);

    boolean saveRolesByUserId(@Param("userId")Integer userId, @Param("roleIds")List <Integer> roleIds);
}
