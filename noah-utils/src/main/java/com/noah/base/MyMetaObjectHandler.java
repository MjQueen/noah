package com.noah.base;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * MP的自动填充类
 */
@Slf4j
@Component //表示将该类放入容器
public class MyMetaObjectHandler implements MetaObjectHandler {


    //使用MP添加操作时，该方法会自动执行，metaObject表示要添加的对象（比如：employee，category...）
    @Override
    public void insertFill(MetaObject metaObject) {
        //对MetaObject对象的属性赋值
        //单属性有值的情况下，不会做自动填充
        //给操作的对象种的createTime属性赋值
        //最底层方法
        metaObject.setValue("createTime", LocalDateTime.now());
        metaObject.setValue("updateTime", LocalDateTime.now());

//        metaObject.setValue("createUser", BaseContext.getCurrentId());
//        metaObject.setValue("updateUser", BaseContext.getCurrentId());
    }


    //使用MP修改操作时，该方法会自动执行，metaObject表示要修改的对象（比如：employee，category...）
    @Override
    public void updateFill(MetaObject metaObject) {
        log.info("当前线程ID：{}", Thread.currentThread().getId());
        //对MetaObject对象的属性赋值

        metaObject.setValue("updateTime", LocalDateTime.now());
//        metaObject.setValue("updateUser", BaseContext.getCurrentId());
    }
}
