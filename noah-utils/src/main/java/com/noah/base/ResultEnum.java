package com.noah.base;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ResultEnum {

    // 成功
    SUCCESS(20000, "成功"),
    // 错误
    ERROR(999, "错误");

    private Integer code;

    public String desc;

}